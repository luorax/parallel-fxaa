### Overview ###

Parallel - FXAA is a C++/OpenMP implementation of the famous image-space anti-aliasing algorithm called 'FXAA'. It's prepared solely for a school course called "Parallel Programming".

### Requirements ###

The project has no external library requirements. You can download the source, build it and run it without having to install or download anything else.

### Building ###

The project uses CMake as its build system. You can either use it to to generate your project files, to build the program, or, you can create your own project file in the IDE of your choice and just import the source files.

### Command line arguments ###

* src <source>: the source file.
* dst <target>: the output file.
* *(optional)* threads <threadCount>: the number of threads to run in. Note that this is automatically clamped based on your system capabilities.
* *(optional)* minBlur <minBlur>: the minimum blur amount in the FXAA algorithm.
* *(optional)* maxBlur <maxBlur>: the maximum blur size in the FXAA algorithm.
* *(optional)* blurMultiplier <multiplier>: the blur size multiplier.

### GLSL reference implementation ###

The project comes with a reference implementation written in the OpenGL Shading Language (GLSL). It's a direct copy of the shader found in the AGE Game Engine, my own work-in-progress 3D rendering and game engine. It's only provided as a proof that it is based on my own work, and is not part of the actual program.

### Sample images ###

The repository contains 3 demo images: a screenshot taken from the AGE Game Engine, the output of the shader on the same screen directly from the engine, and the output of the program provided in this repository. There are some differences between the two images, that is due to the linear filtering used in the engine and the slightly different parameters used.

### Credits ###

* Sean T. Barrett. for stb_image and stb_image_write.
* thebennybox for his reference implementation: http://www.youtube.com/watch?v=Z9bYzpwVINA