cmake_minimum_required(VERSION 2.8.4)
project(ParallelFXAA)

set(PACKAGE_NAME ParallelFXAA)
set(MAJOR_VERSION 0)
set(MINOR_VERSION 0)
set(PATCH_VERSION 1)
set(PACKAGE_VERSION ${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION})

SET(CMAKE_CXX_FLAGS "-fopenmp -O3 -Wall -Wextra")

add_subdirectory(src)