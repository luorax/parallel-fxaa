#include "CFXAA.h"

#include <cmath>

#include <iostream>

CColor CFXAA::applyFilter(int col,int row)
{
    //////////////////////////////////////////////////
    //  Sample the center pixel and its I-neighbours
    CColor texelC=texelFetch(col,row);
    CColor texelBL=texelFetch(col-1,row-1);
    CColor texelBR=texelFetch(col+1,row-1);
    CColor texelTL=texelFetch(col-1,row+1);
    CColor texelTR=texelFetch(col+1,row+1);

    //////////////////////////////////////////////////
    //  Compute the luminance values
    float lumC=texelC.getLuminance();
    float lumBL=texelBL.getLuminance();
    float lumBR=texelBR.getLuminance();
    float lumTL=texelTL.getLuminance();
    float lumTR=texelTR.getLuminance();
    float lumMin=std::min(lumC,std::min(std::min(lumTL,lumTR),std::min(lumBL,lumBR)));
    float lumMax=std::max(lumC,std::max(std::max(lumTL,lumTR),std::max(lumBL,lumBR)));

    //////////////////////////////////////////////////
    //  Compute the blur direction
    float blurDirection[2]={ ((lumTL+lumTR)-(lumBL+lumBR)),((lumTL+lumBL)-(lumTR+lumBR)) };
    float epsilon=std::max(m_minBlur,(lumBL+lumBR+lumTL+lumTR)*0.25f*m_blurMultiplier);
    float dirAdjustment=1.0f/std::max(std::min((float) fabs(blurDirection[0]),(float) fabs(blurDirection[1])),epsilon);

    for (int i=0;i<2;i++)
        blurDirection[i]=std::min(std::max(blurDirection[i]*dirAdjustment,-m_maxBlur),m_maxBlur);

    float weights[4]={ 1.0f/3.0f-0.5f, 2.0f/3.0f-0.5f, -0.5f, 0.5f };

    //////////////////////////////////////////////////
    //  Make 2 different choices for the new color
    CColor results[2];

    results[0]=(
        texelFetch(int(col+blurDirection[0]*weights[0]),int(row+blurDirection[1]*weights[0]))+
        texelFetch(int(col+blurDirection[0]*weights[1]),int(row+blurDirection[1]*weights[1])))*0.5f;

    results[1]=results[0]*0.5f+(
        texelFetch(int(col+blurDirection[0]*weights[2]),int(row+blurDirection[1]*weights[2]))+
        texelFetch(int(col+blurDirection[0]*weights[3]),int(row+blurDirection[1]*weights[3])))*0.25f;

    //////////////////////////////////////////////////
    //  Compute the test luminance
    float lumCheck=results[1].getLuminance();

    //////////////////////////////////////////////////
    //  Choose the 'better' sample
    return (lumCheck<lumMin || lumCheck>lumMax) ? results[0] : results[1];
};