#ifndef H___CFILTER
#define H___CFILTER

#include "CColor.h"

#include "stb_image.h"
#include "stb_image_write.h"

#include <string>

class CFilter
{
public:
    bool apply(const std::string& source,const std::string& target);

    inline void setThreadCount(const int& threadCount) { m_numThreads=threadCount; }
    const int& getThreadCount() const { return m_numThreads; }

protected:
    CFilter() {}

    CColor texelFetch(int col,int row);
    void texelWrite(int col,int row,const CColor& color);

    virtual CColor applyFilter(int col,int row)=0;

protected:
    int m_numThreads;

    stbi_uc* m_sourceFile;
    stbi_uc* m_targetFile;
    int m_width,m_height,m_components;
};

#endif