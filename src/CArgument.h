#ifndef H___CARGUMENT
#define H___CARGUMENT

//////////////////////////////////////////////////
//  Argument types enumeration
enum ArgumentType
{
    ARGUMENT_TYPE_SOURCE_FILE,
    ARGUMENT_TYPE_TARGET_FILE,
    ARGUMENT_TYPE_THREAD_COUNT,
    ARGUMENT_TYPE_MIN_BLUR,
    ARGUMENT_TYPE_MAX_BLUR,
    ARGUMENT_TYPE_BLUR_MULTIPLIER
};

//////////////////////////////////////////////////
//  Argument base class
class CArgument
{
public:
    inline const ArgumentType& getType() const { return m_type; }

protected:
    CArgument(ArgumentType type):
        m_type(type)
    {}

protected:
    ArgumentType m_type;
};

//////////////////////////////////////////////////
//  Source file argument
class CSourceArgument: public CArgument
{
public:
    CSourceArgument(const std::string& filePath):
        CArgument(ARGUMENT_TYPE_SOURCE_FILE),
        m_filePath(filePath)
    {}

    inline const std::string& getSourceFile() const { return m_filePath; }

private:
    std::string m_filePath;
};

//////////////////////////////////////////////////
//  Target file argument
class CTargetArgument: public CArgument
{
public:
    CTargetArgument(const std::string& filePath):
        CArgument(ARGUMENT_TYPE_TARGET_FILE),
        m_filePath(filePath)
    {}

    inline const std::string& getTargetFile() const { return m_filePath; }

private:
    std::string m_filePath;
};

//////////////////////////////////////////////////
//  Thread count argument
class CThreadCountArgument: public CArgument
{
public:
    CThreadCountArgument(const int& threadCount):
        CArgument(ARGUMENT_TYPE_THREAD_COUNT),
        m_threadCount(threadCount)
    {}

    inline const int& getThreadCount() const { return m_threadCount; }

private:
    int m_threadCount;
};

//////////////////////////////////////////////////
//  Minimum blur argument
class CMinBlurArgument: public CArgument
{
public:
    CMinBlurArgument(const float& minBlur):
        CArgument(ARGUMENT_TYPE_MIN_BLUR),
        m_minBlur(minBlur)
    {}

    inline const int& getMinBlur() const { return m_minBlur; }

private:
    int m_minBlur;
};

//////////////////////////////////////////////////
//  Maximum blur argument
class CMaxBlurArgument: public CArgument
{
public:
    CMaxBlurArgument(const float& maxBlur):
        CArgument(ARGUMENT_TYPE_MAX_BLUR),
        m_maxBlur(maxBlur)
    {}

    inline const int& getMaxBlur() const { return m_maxBlur; }

private:
    int m_maxBlur;
};

//////////////////////////////////////////////////
//  Blur size multiplier argument
class CBlurMultiplierArgument: public CArgument
{
public:
    CBlurMultiplierArgument(const float& blurMultiplier):
        CArgument(ARGUMENT_TYPE_BLUR_MULTIPLIER),
        m_blurMultiplier(blurMultiplier)
    {}

    inline const int& getBlurMultiplier() const { return m_blurMultiplier; }

private:
    int m_blurMultiplier;
};

#endif