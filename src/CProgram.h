#ifndef H___CPROGRAM
#define H___CPROGRAM

#include <string>
#include <vector>

class CProgram
{
public:
    void processArgs(const std::vector<std::string>&);
    int execute(int argc,char** argv);
    
public:
    static CProgram& getInstance();
    
private:
    CProgram():
        m_source(""),
        m_target(""),
        m_threads(1),
        m_minBlur(1.0f/128.0f),
        m_maxBlur(8.0f),
        m_blurMultiplier(1.0f/8.0f)
    {}
    
    std::string m_source;
    std::string m_target;
    int m_threads;
    float m_minBlur,m_maxBlur,m_blurMultiplier;
};

#endif