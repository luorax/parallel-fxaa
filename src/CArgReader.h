#ifndef H___CARGREADER
#define H___CARGREADER

#include <string>
#include <vector>

class CArgument;

class CArgReader
{
public:
    CArgReader(const std::vector<std::string>&);
    ~CArgReader();
    
    const std::vector<CArgument*>& getArguments() const;
    
private:
    std::vector<CArgument*> m_args;
};

#endif