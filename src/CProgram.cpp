#include "CProgram.h"
#include "CArgReader.h"
#include "CArgument.h"
#include "CFXAA.h"

#include <iostream>

void CProgram::processArgs(const std::vector<std::string>& args)
{
	//////////////////////////////////////////////////
	//  Create an arg reader object for interpreting
    //  the arguments
    CArgReader argReader=CArgReader(args);
    
	//////////////////////////////////////////////////
	//  Query the interpreted argument list
    const std::vector<CArgument*>& arguments=argReader.getArguments();
    
	//////////////////////////////////////////////////
	//  Process the arguments
    for (std::vector<CArgument*>::const_iterator it=arguments.begin();it!=arguments.end();it++)
    {
        //////////////////////////////////////////////////
        //  Retrieve the argument object
        CArgument* argument=*it;

        //////////////////////////////////////////////////
        //  Process it
        switch (argument->getType())
        {
        case ARGUMENT_TYPE_SOURCE_FILE:
            m_source=((CSourceArgument*) argument)->getSourceFile();
            break;

        case ARGUMENT_TYPE_TARGET_FILE:
            m_target=((CTargetArgument*) argument)->getTargetFile();
            break;

        case ARGUMENT_TYPE_THREAD_COUNT:
            m_threads=((CThreadCountArgument*) argument)->getThreadCount();
            break;

        case ARGUMENT_TYPE_MIN_BLUR:
            m_minBlur=((CMinBlurArgument*) argument)->getMinBlur();
            break;

        case ARGUMENT_TYPE_MAX_BLUR:
            m_maxBlur=((CMaxBlurArgument*) argument)->getMaxBlur();
            break;

        case ARGUMENT_TYPE_BLUR_MULTIPLIER:
            m_blurMultiplier=((CBlurMultiplierArgument*) argument)->getBlurMultiplier();
            break;
        }
    }
}

int CProgram::execute(int argc,char** argv)
{
	//////////////////////////////////////////////////
	//  Store the command line arguments in a vector
    std::vector<std::string> arguments(argc-1,"");
    
    for (int i=1;i<argc;i++)
        arguments[i-1]=std::string(argv[i]);

	//////////////////////////////////////////////////
	//  Process the arguments
    processArgs(arguments);

    //////////////////////////////////////////////////
    //  Create & configure the filter object
    CFXAA filter;

    filter.setThreadCount(m_threads);
    filter.setMinBlurSize(m_minBlur);
    filter.setMaxBlurSize(m_maxBlur);
    filter.setBlurSizeMultiplier(m_blurMultiplier);

    //////////////////////////////////////////////////
    //  Apply the filter
    filter.apply(m_source,m_target);
}

CProgram& CProgram::getInstance()
{
	//////////////////////////////////////////////////
	//  Create the instance here for thread-safety reasons
    static CProgram c_theInstance;
    
    //////////////////////////////////////////////////
	//  Return the instance
    return c_theInstance;
}

int main(int argc,char** argv)
{
    return CProgram::getInstance().execute(argc,argv);
}