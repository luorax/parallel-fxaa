#include "CFilter.h"
#include "CStopWatch.h"

#include <omp.h>
#include <iostream>

bool CFilter::apply(const std::string& source,const std::string& target)
{
    //////////////////////////////////////////////////
    //  Try to load the source file
    m_sourceFile=stbi_load(source.c_str(),&m_width,&m_height,&m_components,4);

    //////////////////////////////////////////////////
    //  Did we succeed?
    if (m_sourceFile==NULL)
    {
        std::cerr << "Unable to open the source file! Reason: " <<stbi_failure_reason() << std::endl;
        return false;
    }

    //////////////////////////////////////////////////
    //  Allocate the target file
    m_targetFile=new stbi_uc[m_width*m_height*4];

    //////////////////////////////////////////////////
    //  Set the number of threads
    int threadCount=std::min(omp_get_max_threads(),m_numThreads);
    std::cout << "Running the filter in " << threadCount << " threads." << std::endl;

    omp_set_num_threads(threadCount);

    //////////////////////////////////////////////////
    //  Create the stopwatch object
    CStopWatch stopWatch;
    stopWatch.start();

    //////////////////////////////////////////////////
    //  Apply the filter
    #pragma omp parallel for
    for (int row=0;row<m_height;row++)
        for (int col=0;col<m_width;col++)
            texelWrite(col,row,applyFilter(col,row));

    stopWatch.pause();

    //////////////////////////////////////////////////
    //  Store the success status here
    bool success=stbi_write_tga(target.c_str(),m_width,m_height,4,m_targetFile)!=0;

    //////////////////////////////////////////////////
    //  Cleanup after the filter
    stbi_image_free(m_sourceFile);
    delete[] m_targetFile;

    //////////////////////////////////////////////////
    //  Return the success variable
    if (success)
        std::cout << "Finished applying the filter in " << stopWatch.getSeconds() << " seconds." << std::endl;
}

#define TEXEL_INDEX(COL,ROW) (((m_height-(ROW)-1)*m_width+(COL))*4)

CColor CFilter::texelFetch(int col,int row)
{
    //////////////////////////////////////////////////
    //  Clamp the UV
    col=std::max(std::min(col,m_width-1),0);
    row=std::max(std::min(row,m_height-1),0);

    //////////////////////////////////////////////////
    //  Calculate the appropriate index
    int texCoord=TEXEL_INDEX(col,row);

    //////////////////////////////////////////////////
    //  Sample the image
    return CColor(m_sourceFile[texCoord]/255.0f,m_sourceFile[texCoord+1]/255.0f,m_sourceFile[texCoord+2]/255.0f,m_sourceFile[texCoord+3]/255.0f);
}

void CFilter::texelWrite(int col,int row,const CColor& color)
{
    //////////////////////////////////////////////////
    //  Make sure we're not trying to write ouf of
    //  the texture
    if (col < 0 || col >= m_width || row < 0 || row >= m_height)
        return;

    //////////////////////////////////////////////////
    //  Calculate the array index
    int texCoord=TEXEL_INDEX(col,row);

    //////////////////////////////////////////////////
    //  Write the image
    m_targetFile[texCoord]=color.m_red*255.0f;
    m_targetFile[texCoord+1]=color.m_green*255.0f;
    m_targetFile[texCoord+2]=color.m_blue*255.0f;
    m_targetFile[texCoord+3]=color.m_alpha*255.0f;
}