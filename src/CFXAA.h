#ifndef H___CFXAA
#define H___CFXAA

#include "CFilter.h"

class CFXAA: public CFilter
{
public:
    CFXAA():
        m_minBlur(1.0f/128.0f),
        m_maxBlur(8.0f),
        m_blurMultiplier(1.0f/8.0f)
    {}

    inline void setMinBlurSize(const float& value) { m_minBlur=value; }
    inline void setMaxBlurSize(const float& value) { m_maxBlur=value; }
    inline void setBlurSizeMultiplier(const float& value) { m_blurMultiplier=value; }

    const float& getMinBlurSize() { return m_minBlur; }
    const float& getMaxBlurSize() { return m_maxBlur; }
    const float& getBlurSizeMultiplier() { return m_blurMultiplier; }

protected:
    CColor applyFilter(int col,int row);

private:
    float m_minBlur,m_maxBlur,m_blurMultiplier;
};

#endif