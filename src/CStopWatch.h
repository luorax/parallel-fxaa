#ifndef H___CSTOPWATCH
#define H___CSTOPWATCH

#include <omp.h>

class CStopWatch
{
public:
	CStopWatch():
        m_started(false),
        m_paused(false),
        m_startTime(0),
        m_pausedTime(0)
    {}
    
	virtual ~CStopWatch() {}

	inline void start()
	{
		if (!m_started)
		{
			m_started=true;
			m_paused=false;

			m_startTime=omp_get_wtime();
		}
	}

	inline void stop()
	{
		if (m_started)
		{
			m_paused=m_started=false;
			m_pausedTime=m_startTime=0;
		}
	}

	inline void pause()
	{
		if (m_started && !m_paused)
		{
			m_paused=true;
			m_pausedTime+=omp_get_wtime()-m_startTime;
		}
	}

	inline void unPause()
	{
		if (m_started && m_paused)
		{
			m_paused=false;
			m_startTime=omp_get_wtime();
		}
	}

	inline bool isPaused() const
	{
		return m_paused;
	}

	inline bool isStarted() const
	{
		return m_started;
	}

	inline bool isRunning() const
	{
		return m_started && !m_paused;
	}

	inline double getSeconds() const
	{
		if (m_started)
			return m_pausedTime+(m_paused ? 0.0 : omp_get_wtime()-m_startTime);

		return 0.0;
	}

private:
	double m_startTime,m_pausedTime;
	bool m_started,m_paused;
};

#endif