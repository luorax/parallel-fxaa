#ifndef H___CCOLOR
#define H___CCOLOR

struct CColor
{
    float m_red;
    float m_green;
    float m_blue;
    float m_alpha;

    CColor() {}
    CColor(const float& red,const float& green,const float& blue,const float& alpha):
        m_red(red),
        m_green(green),
        m_blue(blue),
        m_alpha(alpha)
    {}

    inline float getLuminance() const { return m_red*0.27f+m_green*0.67f+m_blue*0.06f; }

    CColor operator +(const CColor& other) { return CColor(m_red+other.m_red,m_green+other.m_green,m_blue+other.m_blue,m_alpha+other.m_alpha); }
    CColor operator -(const CColor& other) { return CColor(m_red-other.m_red,m_green-other.m_green,m_blue-other.m_blue,m_alpha-other.m_alpha); }
    CColor operator *(const CColor& other) { return CColor(m_red*other.m_red,m_green*other.m_green,m_blue*other.m_blue,m_alpha*other.m_alpha); }

    CColor operator +(const float& lambda) { return CColor(m_red+lambda,m_green+lambda,m_blue+lambda,m_alpha+lambda); }
    CColor operator -(const float& lambda) { return CColor(m_red-lambda,m_green-lambda,m_blue-lambda,m_alpha-lambda); }
    CColor operator *(const float& lambda) { return CColor(m_red*lambda,m_green*lambda,m_blue*lambda,m_alpha*lambda); }
};

#endif