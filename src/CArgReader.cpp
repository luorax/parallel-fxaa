#include "CArgReader.h"
#include "CArgument.h"

#include <iostream>
#include <cstdlib>

CArgReader::CArgReader(const std::vector<std::string>& arguments)
{
	//////////////////////////////////////////////////
	//  Query the argument count and create the current
    //  argument counter
    unsigned argCount=arguments.size();
    unsigned argId=0;

    //////////////////////////////////////////////////
    //  Process the arguments
    while (argId < argCount)
    {
        //////////////////////////////////////////////////
        //  Retrieve the current argument
        const std::string& argument=arguments[argId];

        //////////////////////////////////////////////////
        //  Is the the source file?
        if (argument=="-src")
        {
            if ((argId+1)==argCount)
                std::cerr << "'-src' usage: -src <filePath>" << std::endl;
            else
                m_args.push_back(new CSourceArgument(arguments[argId+1]));

            argId+=2;
        }

        //////////////////////////////////////////////////
        //  Is it the target file?
        else if (argument=="-dst")
        {
            if ((argId+1)==argCount)
                std::cerr << "'-dst' usage: -dst <filePath>" << std::endl;
            else
                 m_args.push_back(new CTargetArgument(arguments[argId+1]));

            argId+=2;
        }

        //////////////////////////////////////////////////
        //  Is it the thread count?
        else if (argument=="-threads")
        {
            if ((argId+1)==argCount)
                std::cerr << "'-threads' usage: -threads <threadCount>" << std::endl;
            else
                m_args.push_back(new CThreadCountArgument(std::atoi(arguments[argId+1].c_str())));

            argId+=2;
        }

        //////////////////////////////////////////////////
        //  Is it the minimum blur size?
        else if (argument=="-minBlur")
        {
            if ((argId+1)==argCount)
                std::cerr << "'-minBlur' usage: -minBlur <minimumBlurSize>" << std::endl;
            else
                m_args.push_back(new CMinBlurArgument(std::atof(arguments[argId+1].c_str())));

            argId+=2;
        }

            //////////////////////////////////////////////////
            //  Is it the maximum blur size?
        else if (argument=="-maxBlur")
        {
            if ((argId+1)==argCount)
                std::cerr << "'-maxBlur' usage: -maxBlur <maximumBlurSize>" << std::endl;
            else
                m_args.push_back(new CMaxBlurArgument(std::atof(arguments[argId+1].c_str())));

            argId+=2;
        }

            //////////////////////////////////////////////////
            //  Is it the blur size multiplier?
        else if (argument=="-blurMultiplier")
        {
            if ((argId+1)==argCount)
                std::cerr << "'-blurMultiplier' usage: -blurMultiplier <blurSizeMultiplier>" << std::endl;
            else
                m_args.push_back(new CBlurMultiplierArgument(std::atof(arguments[argId+1].c_str())));

            argId+=2;
        }

        //////////////////////////////////////////////////
        //  Unknown argument
        else
        {
            std::cerr << "Unknown command line argument: " << argument << std::endl;
            argId++;
        }
    }
}

CArgReader::~CArgReader()
{
	//////////////////////////////////////////////////
	//  Release the argument objects
    for (std::vector<CArgument*>::iterator it=m_args.begin();it!=m_args.end();it++)
        delete *it;
}
    
const std::vector<CArgument*>& CArgReader::getArguments() const
{
    return m_args;
}